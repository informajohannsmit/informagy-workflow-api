﻿using InformagyWorkflowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Services
{
    public interface ISnagScrutineer
    {
        Response SubmitSnagList(SnagList Snags);
    }
}
