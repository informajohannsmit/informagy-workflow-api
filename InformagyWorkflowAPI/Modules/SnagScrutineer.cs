﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Repositories;

namespace InformagyWorkflowAPI.Services
{
    public class SnagScrutineer : ISnagScrutineer
    {
        private IDataRepository dataRepository;
        private IGlobalService globalService;

        public SnagScrutineer(IGlobalService GlobalService)
        {
            globalService = GlobalService;
        }

        public Response SubmitSnagList(SnagList Snags)
        {
            Response response = new Response();

            try
            {
                //Add to database

                //Generate and send pdf
                globalService.SendEmail("johannsmit31@gmail.com", generateSnagListNotification(Snags));
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }

        string generateSnagReport(SnagList snags)
        {
            StringBuilder htmlReport = new StringBuilder();
            htmlReport.Append($"<!doctype html><html lang='en'><head></head><body>");
            htmlReport.Append($"<h1>Snag List</h1><h2>Project: {snags.ProjectID}</h2>");
            htmlReport.Append($"<table>");
            htmlReport.Append($"<tr>");
            snags.Items.ForEach(item => {
                htmlReport.Append($"<td>{item.Area}</td>");
                htmlReport.Append($"<td>{item.DateTime}</td>");
                htmlReport.Append($"<td>{item.Description}</td>");
                htmlReport.Append($"<td><img src='{item.Image}'</td>");
            });
            htmlReport.Append($"</tr>");
            htmlReport.Append($"</table>");
            htmlReport.Append($"</body></html>");

            return htmlReport.ToString();
        }

        string generateSnagListNotification(SnagList snag)
        {
            StringBuilder notification = new StringBuilder();
            notification.Append($"Good Day,\n");
            notification.Append($"\nThis is notify you that a new snag list have been submitted.");
            notification.Append($"\nSubmitted By: ");
            notification.Append($"\nProject: ");

            return notification.ToString();
        }
    }
}
