﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        public bool Active { get; set; }
    }
}
