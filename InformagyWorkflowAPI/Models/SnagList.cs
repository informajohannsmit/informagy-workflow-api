﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Models
{
    public class SnagList
    {
        public int ID { get; set; }
        public int ProjectID { get; set; }
        public List<SnagItem> Items { get; set; }
    }

    public class SnagItem
    {
        public int ID { get; set; }
        public DateTime DateTime { get; set; }
        public string Area { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
    }
}
