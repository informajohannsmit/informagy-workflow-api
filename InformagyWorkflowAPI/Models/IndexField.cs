﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Models
{
    public class IndexField
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string DataType { get; set; }
        public string DefaultValue { get; set; }
        public int MinLength { get; set; }
        public int MaxLenghth { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }
    }
}
