﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Models
{
    public class Response
    {
        public object Data { get; set; }
        public bool ErrorOccurred { get; set; }
        public string ErrorMessage { get; set; }
    }
}
