﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Models
{
    public class SystemValue
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public bool UserDefined { get; set; }
    }
}
