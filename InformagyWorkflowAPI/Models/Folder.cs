﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Models
{
    public class Folder
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ParentFolderID { get; set; }
        public bool Available { get; set; }
        public int CustomerID { get; set; }
        public List<IndexField> Fields { get; set; }
    }
}
