﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Models
{
    public class ModuleFolderAccess
    {
        public int ID { get; set; }
        public int FolderID { get; set; }
        public int ModuleID { get; set; }
    }
}
