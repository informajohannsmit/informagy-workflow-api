﻿using Dapper;
using Dapper.Contrib.Extensions;
using InformagyWorkflowAPI.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Repositories
{
    public class DataRepository : IDataRepository
    {
        private IConfiguration configuration;
        private Func<DbConnection> sqlFactory;

        public DataRepository(IConfiguration Configuration)
        {
            configuration = Configuration;
            sqlFactory = () => new SqlConnection(configuration["DataConnection:ConnectionString"]);
        }

        public SystemUser GetUser(string Username)
        {
            using (var connection = sqlFactory())
            {
                connection.Open();
                return connection.QuerySingleOrDefault<SystemUser>("GetSystemUser", new { Username = Username }, commandType: CommandType.StoredProcedure);
            }
        }

        IEnumerable<T> IDataRepository.GetData<T>(string StoredProcedureName, DynamicParameters parameters)
        {
            using (var connection = sqlFactory())
            {
                connection.Open();
                return connection.Query<T>(StoredProcedureName, commandType: CommandType.StoredProcedure, param: parameters);
            }
        }

        IEnumerable<T> IDataRepository.GetData<T>(string TableName, string WhereClause)
        {
            using (var connection = sqlFactory())
            {
                connection.Open();
                return connection.Query<T>($"SELECT * FROM {TableName} WHERE {WhereClause}");
            }
        }

        public void AddEditDeleteData(string StoredProcedureName, DynamicParameters parameters)
        {
            using (var connection = sqlFactory())
            {
                connection.Open();
                connection.Execute(StoredProcedureName, commandType: CommandType.StoredProcedure, param: parameters);
            }
        }
    }
}