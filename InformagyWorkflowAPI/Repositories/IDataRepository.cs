﻿using Dapper;
using InformagyWorkflowAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Repositories
{
    public interface IDataRepository
    {
        SystemUser GetUser(string Username);

        IEnumerable<T> GetData<T>(string StoredProcedureName, DynamicParameters parameters);
        IEnumerable<T> GetData<T>(string TableName, string WhereClause);

        void AddEditDeleteData(string StoredProcedureName, DynamicParameters parameters);
    }
}
