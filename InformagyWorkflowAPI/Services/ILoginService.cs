﻿using InformagyWorkflowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Services
{
    public interface ILoginService
    {
        LoginResponse ProcessLogin(LoginRequest loginRequest);
    }
}
