﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Repositories;
using Microsoft.Extensions.Configuration;

namespace InformagyWorkflowAPI.Services
{
    public class FolderService : IFolderService
    {
        private IConfiguration configuration;
        private IDataRepository dataRepository;
        private IGlobalService globalService;

        public FolderService(IConfiguration Configuration, IDataRepository DataRepository, IGlobalService GlobalService)
        {
            configuration = Configuration;
            dataRepository = DataRepository;
            globalService = GlobalService;
        }

        public Response GetFolder(int FolderID)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("FolderID", FolderID);
                response.Data = dataRepository.GetData<Folder>("GetSystemFolder", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }

        public Response AddEditFolder(Folder Folder, string CustomerKey)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("FolderID", Folder.ID);
                parameters.Add("FolderName", Folder.Name);
                parameters.Add("FolderDescription", Folder.Description);
                parameters.Add("ParentFolderID", Folder.ParentFolderID);
                parameters.Add("CustomerID", Folder.CustomerID);
                dataRepository.AddEditDeleteData("AddEditSystemFolder", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }

        public Response DeleteFolder(int FolderID)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("FolderID", FolderID);
                dataRepository.AddEditDeleteData("DeleteSystemFolder", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }
    }
}
