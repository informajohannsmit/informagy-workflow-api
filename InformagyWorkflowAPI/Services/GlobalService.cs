﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InformagyWorkflowAPI.Models;
using System.Net.Mail;
using System.Net;
using System.Text;
using log4net;
using System.Reflection;
using System.IO;
using System.Net.Mime;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Http;
using System.IdentityModel.Tokens.Jwt;

namespace InformagyWorkflowAPI.Services
{
    public class GlobalService : IGlobalService
    {
        private readonly IConfiguration configuration;
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public GlobalService(IConfiguration Configuration)
        {
            configuration = Configuration;
        }

        public void HandleError(ref Response Response, Exception Exception)
        {
            Response.ErrorOccurred = true;
            Response.ErrorMessage = Exception.Message;
            //Log Error Here
        }

        public void SendEmail(string ToAdrress, string EmailBody)
        {
            MailMessage mail = new MailMessage(configuration["EmailSettings:username"], ToAdrress);
            SmtpClient client = new SmtpClient();

            client.Port = Convert.ToInt32(configuration["EmailSettings:port"]);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.EnableSsl = Convert.ToBoolean(configuration["EmailSettings:enableSSL"]);
            client.Credentials = new NetworkCredential(configuration["EmailSettings:username"], configuration["EmailSettings:password"]);
            client.Host = configuration["EmailSettings:host"];

            mail.Subject = "Snag Scrutineer";
            mail.Body = EmailBody;

            client.Send(mail);
            log.Info("Email sent.");
        }

        public string GetTokenFromHeader(HttpRequest Request)
        {
            string token = null;
            StringValues requestTokenHeaders;

            Request.Headers.TryGetValue("Authorization", out requestTokenHeaders);

            if (requestTokenHeaders.Count != null || requestTokenHeaders.Count < 0)
                token = requestTokenHeaders[0];

            return token;
        }

        public string GetCustomerKeyFromToken(string Token) {

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = tokenHandler.ReadJwtToken(Token);

            return "";
        }
    }
}
