﻿using Dapper;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Services
{
    public class LoginService : ILoginService
    {
        private IConfiguration configuration;
        private IDataRepository dataRepository;

        public LoginService(IConfiguration Configuration, IDataRepository DataRepository)
        {
            configuration = Configuration;
            dataRepository = DataRepository;
        }

        LoginResponse ILoginService.ProcessLogin(LoginRequest loginRequest)
        {
            LoginResponse loginResponse = new LoginResponse();
            SystemUser systemUser = getSystemUser(loginRequest.Username);

            if (systemUser == null)
            {
                //User not found
                loginResponse.ErrorOccurred = true;
                loginResponse.ErrorMessage = "Username not found";
            }
            else if (systemUser.Locked)
            {
                //User locked
                loginResponse.ErrorOccurred = true;
                loginResponse.ErrorMessage = "User locked";
            }
            else if (systemUser.Password != loginRequest.Password)
            {
                //Incorrect password
                loginResponse.ErrorOccurred = true;
                loginResponse.ErrorMessage = "Incorrect password";
            }
            else
            {
                //Login user OK
                loginResponse.Token = buildToken(systemUser.Username, systemUser.CustomerKey);
            }


            return loginResponse;
        }

        private SystemUser getSystemUser(string username)
        {
            return dataRepository.GetUser(username);
        }

        private string buildToken(string Username, string CustomerKey)
        {
            string issuer = configuration["JWT:issuer"];
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:key"]));
            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            double timeOut = Convert.ToDouble(configuration["JWT:timeout"]);
            List<Claim> claims = new List<Claim>();

            claims.Add(new Claim("Username", Username));
            claims.Add(new Claim("CustomerKey", CustomerKey));

            var token = new JwtSecurityToken(
                issuer: issuer,
                signingCredentials: credentials,
                expires: DateTime.Now.AddMinutes(timeOut),
                claims: claims
            );
     
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
