﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Repositories;

namespace InformagyWorkflowAPI.Services
{
    public class ModuleService : IModuleService
    {
        private IDataRepository dataRepository;
        private IGlobalService globalService;

        public ModuleService(IDataRepository DataRepository, IGlobalService GlobalService)
        {
            dataRepository = DataRepository;
            globalService = GlobalService;
        }

        public Response GetModuleFolderAccess(int ModuleID)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("ModuleID", ModuleID);
                response.Data = dataRepository.GetData<Folder>("GetModuleFolderAccess", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }

        Response IModuleService.AddModuleFolderAccess(ModuleFolderAccess ModuleFolder)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("ModuleID", ModuleFolder.ModuleID);
                parameters.Add("FolderID", ModuleFolder.FolderID);
                dataRepository.AddEditDeleteData("AddModuleFolderAccess", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }

        public Response DeleteModuleFolderAccess(ModuleFolderAccess ModuleFolder)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("ModuleID", ModuleFolder.ModuleID);
                parameters.Add("FolderID", ModuleFolder.FolderID);
                dataRepository.AddEditDeleteData("DeleteModuleFolderAccess", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }
    }
}
