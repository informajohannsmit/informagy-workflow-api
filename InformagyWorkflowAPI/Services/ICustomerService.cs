﻿using InformagyWorkflowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Services
{
    public interface ICustomerService
    {
        Response GetCustomer(int CustomerID);
        Response AddEditCustomer(Customer Customer);
        Response DeactivateCustomer(int CustomerID);
    }
}
