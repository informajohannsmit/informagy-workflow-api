﻿using InformagyWorkflowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Services
{
    public interface IModuleService
    {
        Response GetModuleFolderAccess(int ModuleID);
        Response AddModuleFolderAccess(ModuleFolderAccess ModuleFolder);
        Response DeleteModuleFolderAccess(ModuleFolderAccess ModuleFolder);
    }
}
