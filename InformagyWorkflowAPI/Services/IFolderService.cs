﻿using InformagyWorkflowAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Services
{
    public interface IFolderService
    {
        Response GetFolder(int FolderID);
        Response AddEditFolder(Folder Folder, string CustomerKey);
        Response DeleteFolder(int FolderID);
    }
}