﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Repositories;
using Microsoft.Extensions.Configuration;

namespace InformagyWorkflowAPI.Services
{
    public class CustomerService : ICustomerService
    {

        private IConfiguration configuration;
        private IDataRepository dataRepository;
        private IGlobalService globalService;

        public CustomerService(IConfiguration Configuration, IDataRepository DataRepository, IGlobalService GlobalService)
        {
            configuration = Configuration;
            dataRepository = DataRepository;
            globalService = GlobalService;
        }

        public Response AddEditCustomer(Customer Customer)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("CustomerID", Customer.ID);
                parameters.Add("CustomerName", Customer.Name);
                parameters.Add("CustomerKey", Customer.Key);
                dataRepository.AddEditDeleteData("AddEditCustomer", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }

        public Response DeactivateCustomer(int CustomerID)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("CustomerID", CustomerID);
                dataRepository.AddEditDeleteData("DeactivateCustomer", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }

        public Response GetCustomer(int CustomerID)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("CustomerID", CustomerID);
                response.Data = dataRepository.GetData<Customer>("GetCustomer", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }
    }
}
