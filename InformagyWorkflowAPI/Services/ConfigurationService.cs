﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Repositories;

namespace InformagyWorkflowAPI.Services
{
    public class ConfigurationService : IConfigurationService
    {
        IDataRepository dataRepository;
        IGlobalService globalService;

        public ConfigurationService(IDataRepository DataRepository, IGlobalService GlobalService)
        {
            dataRepository = DataRepository;
            globalService = GlobalService;
        }

        public Response GetSystemValues()
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                response.Data = dataRepository.GetData<SystemValue>("GetSystemValue", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response; 
        }

        public Response AddEditSystemValue(SystemValue SystemValue)
        {
            Response response = new Response();

            try
            {
                if (!SystemValue.UserDefined)
                    SystemValue.UserDefined = true;

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("SystemValueID", SystemValue.ID);
                parameters.Add("Key", SystemValue.Key);
                parameters.Add("Value", SystemValue.Value);
                parameters.Add("Userdefined", SystemValue.UserDefined);
                dataRepository.AddEditDeleteData("AddEditSystemValue", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }

        public Response DeleteSystemValue(int SystemValueID)
        {
            Response response = new Response();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("SystemValueID", SystemValueID);
                dataRepository.AddEditDeleteData("DeleteSystemValue", parameters);
            }
            catch (Exception ex)
            {
                globalService.HandleError(ref response, ex);
            }

            return response;
        }
    }
}
