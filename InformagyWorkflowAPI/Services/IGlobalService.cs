﻿using InformagyWorkflowAPI.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InformagyWorkflowAPI.Services
{
    public interface IGlobalService
    {
        void HandleError(ref Response Response, Exception Exception);
        void SendEmail(string ToAdrress, string EmailBody);
        string GetTokenFromHeader(HttpRequest Request);
        string GetCustomerKeyFromToken(string Token);
    }
}
