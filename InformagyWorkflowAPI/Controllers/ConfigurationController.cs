﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InformagyWorkflowAPI.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Configuration/[action]")]
    public class ConfigurationController : Controller
    {
        IConfigurationService configService;

        public ConfigurationController(IConfigurationService ConfigService)
        {
            configService = ConfigService;
        }

        [ActionName("GetSystemValue")]
        [HttpGet]
        public JsonResult GetSystemValues()
        {
            Response response = configService.GetSystemValues();
            return Json(response);
        }

        [ActionName("AddEditSystemValue")]
        [HttpPost]
        public JsonResult AddEditSystemValue([FromBody] SystemValue SystemValue)
        {
            Response response = configService.AddEditSystemValue(SystemValue);
            return Json(response);
        }

        [ActionName("DeleteSystemValue")]
        [HttpDelete]
        public JsonResult DeleteSystemValue(int SystemValueID)
        {
            Response response = configService.DeleteSystemValue(SystemValueID);
            return Json(response);
        }
    }
}