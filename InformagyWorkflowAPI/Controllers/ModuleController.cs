﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InformagyWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("InformagyWorkflowCorsPolicy")]
    [Produces("application/json")]
    [Route("api/Module/[action]")]
    public class ModuleController : Controller
    {
        private IModuleService moduleService;

        public ModuleController(IModuleService ModuleService)
        {
            moduleService = ModuleService;
        }

        [HttpGet]
        [ActionName("GetModuleFolderAccess")]
        public JsonResult GetModuleFolderAccess(int ModuleID)
        {
            Response response = moduleService.GetModuleFolderAccess(ModuleID);
            return Json(response);
        }

        [HttpPost]
        [ActionName("AddModuleFolderAccess")]
        public JsonResult AddModuleFolderAccess([FromBody]ModuleFolderAccess ModuleFolder)
        {
            Response response = moduleService.AddModuleFolderAccess(ModuleFolder);
            return Json(response);
        }

        [HttpPost]
        [ActionName("DeleteModuleFolderAccess")]
        public JsonResult DeleteModuleFolderAccess([FromBody]ModuleFolderAccess ModuleFolder)
        {
            Response response = moduleService.DeleteModuleFolderAccess(ModuleFolder);
            return Json(response);
        }
    }
}