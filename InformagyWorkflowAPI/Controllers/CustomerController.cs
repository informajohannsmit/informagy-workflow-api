﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InformagyWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("InformagyWorkflowCorsPolicy")]
    [Produces("application/json")]
    [Route("api/Customer/[action]")]
    public class CustomerController : Controller
    {
        private ICustomerService customerService;

        public CustomerController(ICustomerService CustomerService)
        {
            customerService = CustomerService;
        }

        [HttpGet]
        [ActionName("GetCustomer")]
        public JsonResult GetCustomer(int CustomerID)
        {
            Response response = customerService.GetCustomer(CustomerID);
            return Json(response);
        }

        [HttpGet]
        [ActionName("DeactivateCustomer")]
        public JsonResult DeactivateCustomer(int CustomerID)
        {
            Response respose = customerService.DeactivateCustomer(CustomerID);
            return Json(respose);
        }

        [HttpPost]
        [ActionName("AddEditCustomer")]
        public JsonResult AddEditCustomer([FromBody] Customer Customer)
        {
            Response response = customerService.AddEditCustomer(Customer);
            return Json(response);
        }
    }
}