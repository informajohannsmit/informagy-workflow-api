﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace InformagyWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("InformagyWorkflowCorsPolicy")]
    [Produces("application/json")]
    [Route("api/Folder/[action]")]
    public class FolderController : Controller
    {
        private IFolderService folderService;
        private IGlobalService globalService;

        public FolderController(IFolderService FolderService, IGlobalService GlobalService)
        {
            folderService = FolderService;
            globalService = GlobalService;
        }

        [HttpGet]
        [ActionName("GetFolder")]
        public JsonResult GetFolder(int FolderID)
        {
            Response response = folderService.GetFolder(FolderID);
            return Json(response);
        }

        [HttpPost]
        [ActionName("AddEditFolder")]
        public JsonResult AddEditFolder([FromBody]Folder Folder)
        {
            string token = globalService.GetTokenFromHeader(Request);
            string customerKey = globalService.GetCustomerKeyFromToken(token);
            Response response = folderService.AddEditFolder(Folder, customerKey);
            return Json(response);
        }

        [HttpDelete]
        [ActionName("DeleteFolder")]
        public JsonResult DeleteFolder(int FolderID)
        {
            Response response = folderService.DeleteFolder(FolderID);
            return Json(response);
        }
    }
}