﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InformagyWorkflowAPI.Controllers
{
    [EnableCors("InformagyWorkflowCorsPolicy")]
    [Produces("application/json")]
    [Route("api/Login/[action]")]
    public class LoginController : Controller
    {
        private ILoginService loginService;

        public LoginController(ILoginService LoginService)
        {
            loginService = LoginService;
        }

        [HttpPost]
        [ActionName("Login")]
        public JsonResult Login([FromBody]LoginRequest loginRequest)
        {
            return Json(loginService.ProcessLogin(loginRequest));
        }
    }
}