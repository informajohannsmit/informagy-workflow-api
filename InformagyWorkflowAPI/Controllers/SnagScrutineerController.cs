﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InformagyWorkflowAPI.Models;
using InformagyWorkflowAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InformagyWorkflowAPI.Controllers
{
    [Authorize]
    [EnableCors("InformagyWorkflowCorsPolicy")]
    [Produces("application/json")]
    [Route("api/SnagScrutineer/[action]")]
    public class SnagScrutineerController : Controller
    {
        private ISnagScrutineer snagScrutineer;

        public SnagScrutineerController(ISnagScrutineer SnagScrutineer)
        {
            snagScrutineer = SnagScrutineer;
        }

        [HttpPost]
        [ActionName("SubmitSnagList")]
        public JsonResult SubmitSnagList([FromBody]SnagList Snags)
        {
            Response response = snagScrutineer.SubmitSnagList(Snags);
            return Json(response);
        }
    }
}